﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;
using WindowsFormsApplication1.DB.Fluxo_de_Caixa;

namespace WindowsFormsApplication1.DB.Fluxo_de_Caixa
{
    class FluxoDataBase
    {

        public List<FluxoDTO> Consultar(DateTime inicio, DateTime fim)
        {


            string script = @"select * from Consultar_fluxo_de_caixa
            WHERE DataReferencia >= @dt_inicio
                AND DataReferencia <= @dt_fim;";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("dt_inicio", inicio));
            parms.Add(new MySqlParameter("dt_fim", fim));


            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Cosultar_Fluxo> lista = new List<Cosultar_Fluxo>();

            while (reader.Read())
            {
                Cosultar_Fluxo dto = new Cosultar_Fluxo();

                dto.data_entrada = reader.GetString("data_entrada");
                dto.data_saida = reader.GetString("data_saida");
                dto.preco_entrada = reader.GetDecimal("preco_entrada");
                dto.preco_saida = reader.GetDecimal("preco_saida");
                dto.idfluxo_de_caixa = reader.GetInt32("idfluxo_de_caixa");
                dto.Produtos_idProdutos = reader.GetInt32("Produtos_idProdutos");


                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }
        public List<Cosultar_Fluxo> Listar()
        {
            string script = @"select * from vw_consultar_fluxodecaixa";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            Database db = new Database();

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<Cosultar_Fluxo> lista = new List<Cosultar_Fluxo>();

            while (reader.Read())
            {
                Cosultar_Fluxo dto = new Cosultar_Fluxo();


                dto.data_entrada = reader.GetString("data_entrada");
                dto.data_saida = reader.GetString("data_saida");
                dto.preco_entrada = reader.GetDecimal("preco_entrada");
                dto.preco_saida = reader.GetDecimal("preco_saida");
                dto.idfluxo_de_caixa = reader.GetInt32("idfluxo_de_caixa");
                dto.Produtos_idProdutos = reader.GetInt32("Produtos_idProdutos");

                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }

    }

}

        
