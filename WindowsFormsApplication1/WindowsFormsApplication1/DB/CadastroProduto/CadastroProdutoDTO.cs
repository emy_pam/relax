﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Produto
{
    class CadastroProdutoDTO
    {
        public int idProduto { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public DateTime Data_de_vencimento { get; set; }
        public DateTime Data_de_entrada{ get; set; }
        public decimal Valor { get; set; }
        public int IdFornecedor { get; set; }
    }
}
