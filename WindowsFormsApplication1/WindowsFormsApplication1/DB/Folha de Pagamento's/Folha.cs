﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Objeto;

namespace WindowsFormsApplication1.DB.Folha_de_Pagamento_s
{
    public partial class Folha : Form
    {
        public Folha()
        {
            InitializeComponent();
        }

        private void Folha_Load(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void lblCalcular_Click(object sender, EventArgs e)
        {
            decimal Salario = Convert.ToDecimal(txtBase.Text);

            FolhaPagamento vt = new FolhaPagamento();
            decimal t = vt.ValeTransporte(Salario);

            lblVT.Text = t.ToString("F2");

            decimal s_base = Convert.ToDecimal(txtBase.Text);
            int horaExtra = Convert.ToInt32(txtHoraextras.Text);

            FolhaPagamento he = new FolhaPagamento();
            decimal HE = he.HoraExtra1(s_base, horaExtra);

            lblExtra.Text = HE.ToString("F2");


            decimal valorExtra = Convert.ToDecimal(lblExtra.Text);
            decimal faltas = Convert.ToDecimal(txtAtrasos.Text);

            FolhaPagamento ft = new FolhaPagamento();
            decimal falta = ft.CalcularDSR(valorExtra, faltas);

            label14.Text = falta.ToString("F2");



            decimal s_bases = Convert.ToDecimal(txtBase.Text);
            decimal hrExtra = Convert.ToDecimal(lblExtra.Text);
            decimal dsr = Convert.ToDecimal(label14.Text);

            FolhaPagamento inns = new FolhaPagamento();
            decimal inss = inns.CalculoINSS(s_base, hrExtra, dsr);

            label16.Text = inss.ToString("F2");


            decimal sa_base = Convert.ToDecimal(txtBase.Text);
            decimal atrasoss = Convert.ToDecimal(txtAtrasos.Text);
            decimal horExtra = Convert.ToDecimal(txtHoraextras.Text);
            decimal dsr2 = Convert.ToDecimal(label14.Text);
            decimal INSS = Convert.ToDecimal(label16.Text);

            FolhaPagamento dsr3 = new FolhaPagamento();
            decimal drs = dsr3.CalculoImpostoRenda(sa_base, atrasoss, horExtra, dsr2, INSS);

            label15.Text = drs.ToString("F2");


            decimal salarioB = Convert.ToDecimal(txtBase.Text);
            decimal horaextra2 = Convert.ToDecimal(lblExtra.Text);
            decimal dsr10 = Convert.ToDecimal(label15.Text);

            FolhaPagamento ins = new FolhaPagamento();
            decimal fgts = ins.CalculoFGTS(salarioB, horaextra2, dsr10);

            label18.Text = fgts.ToString("F2");

            decimal salarioBase = Convert.ToDecimal(txtBase.Text);
            decimal horaextra10 = Convert.ToDecimal(lblExtra.Text);
            decimal drs20 = Convert.ToDecimal(label14.Text);
            decimal inss10 = Convert.ToDecimal(label16.Text);
            decimal ir = Convert.ToDecimal(label15.Text);
            decimal vt10 = Convert.ToDecimal(lblVT.Text);

            FolhaPagamento calculo = new FolhaPagamento();
            decimal liquido = calculo.CalcularLiquido(salarioBase, horaextra10, drs20, inss10, ir, vt10);

            lblCalcular.Text = liquido.ToString("F2");

        }
    }
}
