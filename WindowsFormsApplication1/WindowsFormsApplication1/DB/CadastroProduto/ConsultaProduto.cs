﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Produto;

namespace WindowsFormsApplication1.DB.CadastroProduto
{
    public partial class ConsultaProduto : UserControl
    {
        public ConsultaProduto()
        {
            InitializeComponent();
        }

        public void CarregarGrid()
        {
            CadastroProdutoBusiness business = new CadastroProdutoBusiness();
            List<CadastroProdutoDTO> dto = business.Consultar(txtnome.Text.Trim());

            dgvFluxo.AutoGenerateColumns = false;
            dgvFluxo.DataSource = dto;


        }


        private void button1_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }
    }
}
