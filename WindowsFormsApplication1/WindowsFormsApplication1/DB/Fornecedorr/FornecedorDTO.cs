﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Fornecedorr
{
    class FornecedorDTO
    {
        public int id { get; set; }
        public string Nome_fornecedor { get; set; }
        public string Endereco { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Cidade { get; set; }
        public string CEP { get; set; }
        public string CNPJ { get; set; }

    }
}
