﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Serviços
{
    class ServiçoDTO
    {
        public int idServicos { get; set; }
        public string Tipo_Servicos { get; set; }
        public string Valor_Servico { get; set; }
        public string Descricao { get; set; }
        public string Atendimento { get; set; }
        public string Nome { get; set; }



    }
}
