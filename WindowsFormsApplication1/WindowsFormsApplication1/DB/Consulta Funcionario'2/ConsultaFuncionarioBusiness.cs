﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Consulta_Funcionario_2
{
    class ConsultaFuncionarioBusiness
    {
        public int Salvar(ConsultaFuncionarioDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            ConsultaFuncionarioDTO cat = this.ConsultarPorNome(dto.Nome);
            if (cat != null)
            {
                throw new ArgumentException("Categoria já existe no sistema.");
            }

            ConsultaFuncionarioDatabase db = new ConsultaFuncionarioDatabase();
            return db.Salvar(dto);
        }


        public ConsultaFuncionarioDTO ConsultarPorNome(string nome)
        {
            ConsultaFuncionarioDatabase db = new ConsultaFuncionarioDatabase();
            return db.ConsultarPorNome(nome);
        }


        public List<ConsultaFuncionarioDTO> Consultar(string nome)
        {
            ConsultaFuncionarioDatabase db = new ConsultaFuncionarioDatabase();
            return db.Consultar(nome);
        }


        public List<ConsultaFuncionarioDTO> Listar()
        {
            ConsultaFuncionarioDatabase db = new ConsultaFuncionarioDatabase();
            return db.Listar();
        }
    }
}
