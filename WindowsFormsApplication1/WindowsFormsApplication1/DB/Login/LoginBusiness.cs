﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Login
{
    class LoginBusiness
    {
        LoginDatabase db = new LoginDatabase();

        public bool Logar(string login, string senha)
        {
            bool logou = db.Logar(login, senha);

            return logou;

        }

    }
}
