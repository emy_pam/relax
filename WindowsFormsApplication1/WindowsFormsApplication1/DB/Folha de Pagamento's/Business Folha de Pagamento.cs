﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Folha_de_Pagamento_s
{
    class Business_Folha_de_Pagamento
    {
        public int Salvar(FolhadePagamentoDTO dto)
        {
            if (dto.Nome_funcionario == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            FolhadePagamentoDTO cat = this.ConsultarPorNome(dto.Nome_funcionario);
            if (cat != null)
            {
                throw new ArgumentException("Funcionario já existe no sistema.");
            }

            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.Salvar(dto);
        }


        public FolhadePagamentoDTO ConsultarPorNome(string nome)
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.ConsultarPorNome(nome);
        }

        public List<FolhadePagamentoDTO> Listar()
        {
            FolhadePagamentoDatabase db = new FolhadePagamentoDatabase();
            return db.Listar();
        }


    }
}
