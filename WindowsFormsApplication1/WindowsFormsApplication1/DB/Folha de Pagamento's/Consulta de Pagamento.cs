﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Consulta_Funcionario_2;

namespace WindowsFormsApplication1
{
    public partial class UserControl1 : UserControl
    {
        public UserControl1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                ConsultaFuncionarioBusiness business = new ConsultaFuncionarioBusiness();
                List<ConsultaFuncionarioDTO> lista = business.Consultar(txtBuscar.Text.Trim());

                dvgFuncionario.AutoGenerateColumns = false;
                dvgFuncionario.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "RELAX",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "RELAX",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ConsultaFuncionarioDTO dto = new ConsultaFuncionarioDTO();
                dto.Nome = txtBuscar.Text.Trim();

                ConsultaFuncionarioBusiness business = new ConsultaFuncionarioBusiness();
                business.Salvar(dto);

                MessageBox.Show("Categoria salva com sucesso.", "RELAX",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "RELAX",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "RELAX",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
    

