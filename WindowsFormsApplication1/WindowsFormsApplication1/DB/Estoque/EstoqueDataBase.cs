﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;
using WindowsFormsApplication1.DB.Estoque;

namespace WindowsFormsApplication1.DB.Controle_de_estoque
{
    class EstoqueDataBase
    {

        public int Salvar(EstoqueDTO dto)
        {
            string script = @"INSERT INTO tb_produto (idEstoque, Data_entrega_produto, organizacao_do_produto, Tipo_de_produto, Quantidade_produto, Observaçoes,  Fornecedores, Fornecedores_idFornecedores, Produtos_idProdutos) 
                                   VALUES (@idEstoque, @Data_entrega_produto, @organizacao_do_produto, @Tipo_de_produto, @Quantidade_produto, @ Observaçoes, @Fornecedores, @Fornecedores_idFornecedores, @Produtos_idProdutos)";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("idEstoque", dto.idEstoque));
            parms.Add(new MySqlParameter(" Data_entrega_produto", dto.Data_entrega_produto));
            parms.Add(new MySqlParameter("organizacao_do_produto", dto.organizacao_do_produto));
            parms.Add(new MySqlParameter("Tipo_de_produto", dto.Tipo_de_produto));
            parms.Add(new MySqlParameter(" Data_entrega_produto", dto.Data_entrega_produto));
            parms.Add(new MySqlParameter("Quantidade_produto", dto.Quantidade_produto));
            parms.Add(new MySqlParameter(" Observaçoes", dto.Observaçoes));
            parms.Add(new MySqlParameter("Fornecedores", dto.Fornecedores));
            parms.Add(new MySqlParameter("Fornecedores_idFornecedores", dto.Fornecedores_idFornecedores));
            parms.Add(new MySqlParameter("Produtos_idProdutos", dto.Produtos_idProdutos));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        internal List<EstoqueDTO> Consultar(object estoque)
        {
            throw new NotImplementedException();
        }

        public void Alterar(EstoqueDTO dto)
        {
            string script = @"UPDATE tb_estoque 
                                SET idEstoque = @idEstoque,
                                    Data_entrega_produto  = @Data_entrega_produto,
                                    organizacao_do_produto  = @organizacao_do_produto,
                                    Tipo_de_produto  = @Tipo_de_produto,
                                    Data_entrega_produto = @Data_entrega_produto,
                                    Quantidade_produto  = @Quantidade_produto,
                                    Observaçoes  = @Observaçoes,
                                     Fornecedores = @Fornecedores,
                                      Fornecedores_idFornecedores= @Fornecedores_idFornecedores,
                                    Produtos_idProdutos  = @Produtos_idProdutos,



                                WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("Data_entrega_produto", dto.Data_entrega_produto));
            parms.Add(new MySqlParameter("organizacao_do_produto", dto.organizacao_do_produto));

            parms.Add(new MySqlParameter("Tipo_de_produto", dto.Tipo_de_produto));
            parms.Add(new MySqlParameter("Data_entrega_produto", dto.Data_entrega_produto));

            parms.Add(new MySqlParameter("Quantidade_produto", dto.Quantidade_produto));
            parms.Add(new MySqlParameter("Observaçoes", dto.Observaçoes));

            parms.Add(new MySqlParameter("Fornecedores", dto.Fornecedores));
            parms.Add(new MySqlParameter("Fornecedores_idFornecedores", dto.Fornecedores_idFornecedores));

            parms.Add(new MySqlParameter("Produtos_idProdutos", dto.Produtos_idProdutos));
         




            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_estoque WHERE id_estoque = @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<EstoqueDTO> Consultar(string estoque)
        {
            string script = @"SELECT * FROM tb_estoque WHERE id_estoque like @id_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_estoque", estoque + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EstoqueDTO> Listar = new List<EstoqueDTO>();
            while (reader.Read())
            {

                EstoqueDTO dto = new EstoqueDTO();
                dto.idEstoque = reader.GetInt32("id_estoque");
                dto.Data_entrega_produto = reader.GetString("Data_entrega_produto");

                dto.organizacao_do_produto = reader.GetString("organizacao_do_produto");
                dto.Tipo_de_produto = reader.GetString("Tipo_de_produto");

                dto.Data_entrega_produto = reader.GetString("Data_entrega_produto");
                dto.Quantidade_produto = reader.GetInt32("Quantidade_produto");

                dto.Observaçoes = reader.GetString("Observaçoes");
                dto.Fornecedores = reader.GetString("Fornecedores");

                dto.Fornecedores_idFornecedores = reader.GetInt32("Fornecedores_idFornecedores");
                dto.Produtos_idProdutos = reader.GetInt32("Produtos_idProdutos");

                Listar.Add(dto);
            }
            reader.Close();

            return Listar;

        }

       

    }
}
