﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Login
{
    class LoginDTO
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }

    }
}
