﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Folha_de_Pagamento_s
{
    class FolhadePagamentoDatabase
    {
        public int Salvar(FolhadePagamentoDTO dto)
        {
            string script = @"INSERT INTO Folha_de_Pagamento (nm_nome_funcionario, ds_sexo, ds_inss, ds_irrf, vr_vale_refeicao, vt_vale_transporte, ds_salario_liquido, ds_salario_bruto, ds_atrasos_faltas, ds_descontos_totais, ds_cargo_funcionario, ds_departamento_funcionario, ds_horas_extras ) 
                                   VALUES (@nm_nome_funcionario, @ds_sexo, @ds_inss, @ds_irrf, @vr_vale_refeicao, @vt_vale_transporte, @ds_salario_liquido, @ds_salario_bruto, @ds_atrasos_faltas, @ds_descontos_totais, @ds_cargo_funcionario, @ds_departamento_funcionario, @ds_horas_extras)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_funcionario", dto.Nome_funcionario));
            parms.Add(new MySqlParameter("ds_salario_liquido", dto.Salario_liquido));
            parms.Add(new MySqlParameter("nm_descontos_totais", dto.descontos_totais));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FolhadePagamentoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FolhadePagamentoDTO> lista = new List<FolhadePagamentoDTO>();
            while (reader.Read())
            {
                FolhadePagamentoDTO dto = new FolhadePagamentoDTO();
                dto.Id = reader.GetInt32("idFolhadePagamento");
                dto.Nome_funcionario = reader.GetString("nm_nome_funcionario");
                dto.Salario_base = reader.GetDecimal("ds_salario_base");
                dto.Salario_liquido = reader.GetDecimal("ds_salario_liquido");



                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
        public FolhadePagamentoDTO ConsultarPorNome(string nome_funcionario)
        {
            string script = @"SELECT * FROM tb_categoria WHERE nm_categoria = @nm_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome_funcionario", nome_funcionario));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            FolhadePagamentoDTO dto = null;
            while (reader.Read())
            {
                dto = new FolhadePagamentoDTO();
                dto.Id = reader.GetInt32("id_categoria");
                dto.Nome_funcionario = reader.GetString("nm_nome_funcionario");
                dto.Salario_base = reader.GetDecimal("ds_salario_base");
                dto.Salario_liquido = reader.GetDecimal("ds_dalario_liquido");
            }
            reader.Close();

            return dto;
        }
    }
}
