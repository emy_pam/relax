﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Funcionario
{
    class FuncionarioDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string sexo { get; set; }
        public string Naturalidade { get; set; }
        public DateTime Data_de_nascimento { get; set; }
        public DateTime Data_de_admissao { get; set; }
        public int Telefone { get; set; }
        public string Endereco { get; set; }
        public int CPF_RG { get; set; }
        public DateTime Data_de_demissao { get; set; }
        public int Carteira_de_trabalho { get; set; }
        public string login { get; set; }
        public string senha { get; set; }
        public decimal salarioBase { get; set; }
        public decimal salarioLiquido { get; set; }
        public int Cargo_idCargo { get; set; }
        public bool permissao_adm { get; set; }
        public bool Permissao_usuario { get; set; }
        public bool Permissao_funcionario { get; set; }
       
    }
}
