﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Fornecedorr;
using WindowsFormsApplication1.DB.Produto;

namespace WindowsFormsApplication1
{
    public partial class Cadastro_de_Produto : Form
    {
        public Cadastro_de_Produto()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FornecedorBusiness business = new FornecedorBusiness();
            List<FornecedorDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(FornecedorDTO.id);
            comboBox1.DisplayMember = nameof(FornecedorDTO.Nome_fornecedor);
            comboBox1.DataSource = lista;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }

        private void Cadastro_de_Produto_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FornecedorDTO dto2 = comboBox1.SelectedItem as FornecedorDTO;

                CadastroProdutoDTO dto = new CadastroProdutoDTO();
                dto.IdFornecedor = dto2.id;
                dto.Nome = textBox4.Text;
                dto.Tipo = textBox2.Text;
                dto.Data_de_entrada = dateTimePicker1.Value;
                dto.Data_de_vencimento = dateTimePicker2.Value;
                dto.Valor = numericUpDown1.Value;

                CadastroProdutoBusiness business = new CadastroProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Produto Registrado com sucesso!", "Relax", MessageBoxButtons.OK);

                this.Hide();
                frmInicial tela = new frmInicial();
                tela.Show();
            
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show("Ocorreu um erro ao cadastrar o produto" + ex.Message + "Relax");
            }

          

 
        }
    }
}
