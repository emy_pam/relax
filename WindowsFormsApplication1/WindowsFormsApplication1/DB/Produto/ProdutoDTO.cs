﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Produto
{
    class ProdutoDTO
    {

        public int idProdutos { get; set; }
        public string Nome { get; set; }
        public string Tipo { get; set; }
        public string preco { get; set; }
        public int IdFornecedores { get; set; }


    }
}
