﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Produto
{
    class CadastroProdutoDataBase
    { 
        public int Salvar (CadastroProdutoDTO dto)
        {
            string script = @"Insert into Produtos2 (Nome,Tipo,Data_de_entrada,Data_de_vencimento,Valor,IdFornecedor)
                                     Values (@Nome,@Tipo,@Data_de_entrada,@Data_de_vencimento,@Valor,@IdFornecedor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Tipo", dto.Tipo));
            parms.Add(new MySqlParameter("Data_de_entrada", dto.Data_de_entrada));
            parms.Add(new MySqlParameter("Data_de_vencimento", dto.Data_de_vencimento));
            parms.Add(new MySqlParameter("Valor", dto.Valor));
            parms.Add(new MySqlParameter("IdFornecedor", dto.IdFornecedor));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<CadastroProdutoDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM Produtos2 WHERE Nome like @Nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CadastroProdutoDTO> lista = new List<CadastroProdutoDTO>();
            while (reader.Read())
            {
                CadastroProdutoDTO dto = new CadastroProdutoDTO();
                dto.idProduto = reader.GetInt32("idProduto");
                dto.Nome = reader.GetString("Nome");
                dto.Tipo = reader.GetString("Tipo");
                dto.Data_de_entrada = reader.GetDateTime("Data_de_entrada");
                dto.Data_de_vencimento = reader.GetDateTime("Data_de_vencimento");
                dto.Valor = reader.GetDecimal("Valor");
                dto.IdFornecedor = reader.GetInt32("IdFornecedor");


                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }

        public List<CadastroProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM Produtos2";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CadastroProdutoDTO> lista = new List<CadastroProdutoDTO>();
            while (reader.Read())
            {
                CadastroProdutoDTO dto = new CadastroProdutoDTO();
                dto.idProduto = reader.GetInt32("idProduto");
                dto.Nome = reader.GetString("Nome");
                dto.Tipo = reader.GetString("Tipo");
                dto.Data_de_entrada = reader.GetDateTime("Data_de_entrada");
                dto.Data_de_vencimento = reader.GetDateTime("Data_de_vencimento");
                dto.Valor = reader.GetDecimal("Valor");
                dto.IdFornecedor = reader.GetInt32("IdFornecedor");


                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }






    }
}
