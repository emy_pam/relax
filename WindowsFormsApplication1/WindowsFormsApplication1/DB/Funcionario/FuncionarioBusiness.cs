﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Funcionario
{
    class FuncionarioBusiness
    {
        
        public int Salvar (FuncionarioDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }
            if (dto.login == string.Empty)
            {
                throw new ArgumentException("Login é obrigatório.");
            }
            if (dto.senha == string.Empty)
            {
                throw new ArgumentException("Senha é obrigatória.");
            }
            if (dto.Data_de_nascimento == null)
            {
                throw new ArgumentException("Data de nascimento é obrigatória.");
            }
            if (dto.Data_de_admissao == null)
            {
                throw new ArgumentException("Data de adimissão é obrigatória.");
            }
            if (dto.Telefone == null)
            {
                throw new ArgumentException("Número do telefone é obrigatório.");
            }
            if (dto.sexo == null)
            {
                throw new ArgumentException("Gênero é obrigatório.");
            }
            if (dto.Naturalidade == string.Empty)
            {
                throw new ArgumentException("Naturalidade é obrigatória.");
            }
            if (dto.CPF_RG == null)
            {
                throw new ArgumentException("CPF/RG é obrigatório");
            }
            if (dto.salarioBase == null)
            {
                throw new ArgumentException("Salário é obrigatório.");
            }
            if (dto.Cargo_idCargo == null)
            {
                throw new ArgumentException("Cargo é obrigatório.");
            }

            FuncionarioDatabase db = new FuncionarioDatabase();
            return db.Salvar(dto);
        }

     
    }

}
