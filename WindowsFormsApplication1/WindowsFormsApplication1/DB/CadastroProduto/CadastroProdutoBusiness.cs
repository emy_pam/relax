﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Produto
{
    class CadastroProdutoBusiness
    {
       public int Salvar (CadastroProdutoDTO dto)
       {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.Tipo == string.Empty)
            {
                throw new ArgumentException("Tipo é obrigatório.");
            }

     
            CadastroProdutoDataBase db = new CadastroProdutoDataBase();
            return db.Salvar(dto);
            
       }

       public List<CadastroProdutoDTO> Consultar(string produto)
       {
            CadastroProdutoDataBase db = new CadastroProdutoDataBase();
            return db.Consultar(produto);

       }

       public List<CadastroProdutoDTO> Listar()
       {
            CadastroProdutoDataBase db = new CadastroProdutoDataBase();
            return db.Listar();

       }


    }
}
