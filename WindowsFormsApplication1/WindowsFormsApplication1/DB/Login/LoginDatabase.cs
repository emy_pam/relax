﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Login
{
    class LoginDatabase
    {
        public bool Logar(string login, string senha)
        {
            string script = @"SELECT * FROM Login WHERE Login = @Login AND Senha = @Senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Login", login));
            parms.Add(new MySqlParameter("Senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }



        }
    }
}
