﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Controle_de_estoque;
using WindowsFormsApplication1.DB.Estoque;

namespace WindowsFormsApplication1.DB.Controle_de_estoque
{
    class EstoqueBusiness
    {
        public int Salvar(EstoqueDTO dto)
        {

            EstoqueDataBase db = new EstoqueDataBase();

            if (dto.Quantidade_produto <= 0)
            {
                throw new ArgumentException("Quantidade de Estoque Mínimo é obrigatório");
            }
            if (dto.Quantidade_produto <= 0)
            {
                throw new ArgumentException("Quantidade de Estoque Máximo é obrigatório");
            }

            return db.Salvar(dto);
        }

        public void Alterar(EstoqueDTO dto)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            db.Alterar(dto);
        }
        public void Remover(int id)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            db.Remover(id);
        }
        
        public List<EstoqueDTO> Consultar(string estoque)
        {
            EstoqueDataBase db = new EstoqueDataBase();
            return db.Consultar(estoque);
        }



    }
}
