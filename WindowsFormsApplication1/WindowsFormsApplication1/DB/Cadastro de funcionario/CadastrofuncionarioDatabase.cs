﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Cadastro_de_funcionario
{
    class CadastrofuncionarioDatabase
    {
        public int Salvar (CadastrofuncionarioDTO dto)
        {
            string script = @"
INSERT INTO `salao`.`Cadastro_funcionario` (`Nome`,`Login`, `senha`, `Data_de_nascimento`, `Data_de_admissao`, `Telefone`, `sexo`, `Naturalidade`, `CPF`, `permissao_adm`, `permissao_usuario`, `permissao_funcionario`, `salario_recebido`, `Cargos_idCargos`) 
                                     VALUES ('@Nome', '@Login ', '@senha', '@Data_de_nascimento', '@Data_de_admissao', '@Telefone', '@sexo', '@Naturalidade', '@CPF/RG', '@permissao_adm', '@permissao_usuario', '@permissao_funcionario', '@salario_recebido', @Cargos_idCargos);";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Login", dto.Login));
            parms.Add(new MySqlParameter("Senha", dto.Senha));
            parms.Add(new MySqlParameter("Data_de_nascimento", dto.Data_de_nascimento));
            parms.Add(new MySqlParameter("Data_de_admissao", dto.Data_de_admissao));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("Naturalidade", dto.Naturalidade));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("permissao_adm", dto.Permissao_adm));
            parms.Add(new MySqlParameter("permissao_usuario", dto.Permissao_usuario));
            parms.Add(new MySqlParameter("permissao_funcionario", dto.Prmissao_funcionario));
            parms.Add(new MySqlParameter("salario_recebido", dto.salario_recebido));
            parms.Add(new MySqlParameter("Cargos_idCargos", dto.Cargos_id_cargos));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }
}
