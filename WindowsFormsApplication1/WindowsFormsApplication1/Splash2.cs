﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Splash2 : Form
    {
        public Splash2()
        {
            InitializeComponent();

            Task.Factory.StartNew(() =>
            {

                System.Threading.Thread.Sleep(5000);

                Invoke(new Action(() =>
                {

                    Form3 frm = new Form3();
                    frm.Show();
                    Hide();
                }));
            });
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Splash2_Load(object sender, EventArgs e)
        {

        }
    }
}
