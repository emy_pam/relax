﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Cadastro_de_funcionario
{
    class CadastrofuncionarioDTO
    {
        public int idCadastrar_funcionario { get; set; }
        public string Nome { get; set; }
        public string Data_de_nascimento { get; set; }
        public string Data_de_admissao { get; set; }
        public string Telefone { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string sexo { get; set; }
        public string Naturalidade { get; set; }
        public string CPF { get; set; }
        public string Carteira_de_trabalho { get; set; }
        public bool Permissao_adm { get; set; }
        public bool Permissao_usuario { get; set; }
        public bool Prmissao_funcionario { get; set; }
        public string salario_recebido { get; set; }
        public string Cargos_id_cargos { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }

        

         
    }
}
