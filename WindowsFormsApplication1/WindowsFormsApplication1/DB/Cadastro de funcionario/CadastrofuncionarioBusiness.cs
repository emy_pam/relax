﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Funcionario;

namespace WindowsFormsApplication1.DB.Cadastro_de_funcionario
{
    class CadastrofuncionarioBusiness
    {
        public int Salvar (CadastrofuncionarioDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentNullException("Nome é obrigatório.");

            }
            if (dto.Email == string.Empty)
            {
                throw new ArgumentNullException("Email é obrigsatório.");
            }
            if (dto.Endereco == string.Empty)
            {
                throw new ArgumentNullException("Endereço é obrigatório.");
            }
         if (dto.CPF == string.Empty)
            {
                throw new ArgumentException("CPF é obrigatório");
            }
            if (dto.Carteira_de_trabalho == string.Empty)
            {
                throw new ArgumentException("Carteira de trabalho é obrigatória.");
            }
            if (dto.Naturalidade == string.Empty)
            {
                throw new ArgumentException("Naturalidade é obrigatória.");
            }
            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }
            if (dto.Cargos_id_cargos == string.Empty)
            {
                throw new ArgumentException("Cargo é obrigatório.");
            }
            if (dto.Data_de_admissao == string.Empty)
            {
                throw new ArgumentException("Data de admissão é obrigatória.");
            }
            if (dto.Data_de_nascimento == string.Empty)
            {
                throw new ArgumentException("Data de nascimento é obrigatória.");
            }
            if (dto.sexo == string.Empty)
            {
                throw new ArgumentException("Sexo é obrigatório.");
            }
            if (dto.salario_recebido == string.Empty)
            {
                throw new ArgumentException("Salário é obrigatório");
            }
            CadastrofuncionarioDatabase db = new CadastrofuncionarioDatabase();
            return db.Salvar(dto);

        }

        internal void Salvar(FuncionarioDTO dto)
        {
            throw new NotImplementedException();
        }
    }
}
