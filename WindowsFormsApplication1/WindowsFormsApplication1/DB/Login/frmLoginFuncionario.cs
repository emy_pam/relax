﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.Funcionario;
using MySql.Data.MySqlClient;
using WindowsFormsApplication1.DB.Login;

namespace WindowsFormsApplication1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            string login = txtLogin.Text;
            string senha = txtSenha.Text;

            LoginBusiness business = new LoginBusiness();
            bool logou = business.Logar(login, senha);

            if (logou == true)
            {
                frmInicial tela = new frmInicial();
                tela.Show();
                Hide();
            }
            else
            {
                MessageBox.Show("Credenciais inválidas", "RELAX", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cadastro_de_FuncionarioNovo tela = new Cadastro_de_FuncionarioNovo();
            tela.Show();
            Hide();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }
    }
}
