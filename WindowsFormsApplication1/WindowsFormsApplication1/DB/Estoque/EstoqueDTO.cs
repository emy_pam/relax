﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Estoque
{
    class EstoqueDTO
    {
        public int idEstoque { get; set; }
        public string Data_entrega_produto { get; set; }

        public string organizacao_do_produto { get; set; }
        public string Tipo_de_produto { get; set; }
        public int Quantidade_produto { get; set; }
        public string Observaçoes { get; set; }
        public string Fornecedores { get; set; }
        public int Fornecedores_idFornecedores { get; set; }
        public int Produtos_idProdutos { get; set; }


    }
}
