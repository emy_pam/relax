﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Serviço
{
    class ServicoDataBase
    {
        public int Salvar(ServocoDTO dto)

        {
            string script = @"insert into Servicos (  Tipo_servicos, Valor_servico, Descricao, Atendimento)
                                       values ( @Tipo_servico, @Valor_servico, @Descricao, @Atendimento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Tipo_servico", dto.Tipo_servicos));
            parms.Add(new MySqlParameter("Valor_servico", dto.Valor_servico));
            parms.Add(new MySqlParameter("Descricao", dto.Descricao));
            parms.Add(new MySqlParameter("Atendimento", dto.Atendimento));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(ServocoDTO dto)

        {
            string script = @"UPDATE Servicos 
                                 SET  Tipo_servicos = @Tipo_servicos,
                                      Atendimento  = @Atendimento,
                                      Descricao = @Descricao,
                                      Valor_servico = @Valor_servico,
                                      
                               WHERE idServicos = @idServicos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Tipo_servicos", dto.Tipo_servicos));
            parms.Add(new MySqlParameter("Valor_servico", dto.Valor_servico));
            parms.Add(new MySqlParameter("Descricao", dto.Descricao));
            parms.Add(new MySqlParameter("Atendimento", dto.Atendimento));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int idServicos)
        {
            string script = @"DELETE FROM Servicos WHERE idFornecedores = @idServicos";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idServicos", idServicos));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

















    }
}
