﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;
using WindowsFormsApplication1.DB.Funcionario;

namespace WindowsFormsApplication1.DB.Funcionario
{
    class FuncionarioDatabase
    {
     public int Salvar (FuncionarioDTO dto)
        {
            string script =
                 @"INSERT INTO `salao`.`Cadastro_funcionario` (`Nome`,`Login`, `senha`, `Data_de_nascimento`, `Data_de_admissao`, `Telefone`, `sexo`, `Naturalidade`, `CPF/RG`, `permissao_adm`, `permissao_usuario`, `permissao_funcionario`, `salario_recebido`, `Cargos_idCargos`) 
                                               VALUES ('@Nome', '@Login ', '@senha', '@Data_de_nascimento', '@Data_de_adimissao', '@Telefone', '@sexo', '@Naturalidade', '@CPF/RG', '@permissao_adm', '@permissao_usuario', '@permissao_funcionario', '@salario_recebido', @Cargos_idCargos)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Login", dto.login));
            parms.Add(new MySqlParameter("senha", dto.senha));
            parms.Add(new MySqlParameter("Data_de_nascimento", dto.Data_de_nascimento));
            parms.Add(new MySqlParameter("Data_de_admissao", dto.Data_de_admissao));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("sexo", dto.sexo));
            parms.Add(new MySqlParameter("Naturalidade", dto.Naturalidade));
            parms.Add(new MySqlParameter("CPF_RG", dto.CPF_RG));
            parms.Add(new MySqlParameter("permissao_adm", dto.permissao_adm));
            parms.Add(new MySqlParameter("permissao_usuario", dto.Permissao_usuario));
            parms.Add(new MySqlParameter("permissao_funcionario", dto.Permissao_funcionario));
            parms.Add(new MySqlParameter("salario_recebido", dto.salarioBase));
            parms.Add(new MySqlParameter("Cargos_idCargos", dto.Cargo_idCargo));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);














        }
    }
}
