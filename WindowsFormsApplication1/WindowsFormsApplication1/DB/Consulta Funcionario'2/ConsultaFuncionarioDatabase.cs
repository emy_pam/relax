﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Consulta_Funcionario_2
{
    class ConsultaFuncionarioDatabase
    {
        public int Salvar(ConsultaFuncionarioDTO dto)
        {
            string script = @"INSERT INTO Consulta_Funcionario (nm_categoria) 
                                   VALUES (@nm_categoria)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", dto.Nome));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ConsultaFuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM Consulta_Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ConsultaFuncionarioDTO> lista = new List<ConsultaFuncionarioDTO>();
            while (reader.Read())
            {
                ConsultaFuncionarioDTO dto = new ConsultaFuncionarioDTO();
                dto.id = reader.GetInt32("id_categoria");
                dto.Nome = reader.GetString("nm_categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }


        public List<ConsultaFuncionarioDTO> Consultar(string nome)
        {
            string script = @"SELECT * FROM Consulta_Funcionario WHERE nm_categoria LIKE @nm_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", "%" + nome + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ConsultaFuncionarioDTO> lista = new List<ConsultaFuncionarioDTO>();
            while (reader.Read())
            {
                ConsultaFuncionarioDTO dto = new ConsultaFuncionarioDTO();
                dto.id = reader.GetInt32("id_categoria");
                dto.Nome = reader.GetString("nm_categoria");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public ConsultaFuncionarioDTO ConsultarPorNome(string nome)
        {
            string script = @"SELECT * FROM Consulta_Funcionario WHERE nm_categoria = @nm_categoria";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_categoria", nome));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ConsultaFuncionarioDTO dto = null;
            while (reader.Read())
            {
                dto = new ConsultaFuncionarioDTO();
                dto.id = reader.GetInt32("id_categoria");
                dto.Nome = reader.GetString("nm_categoria");
            }
            reader.Close();

            return dto;
        }
    }
}
