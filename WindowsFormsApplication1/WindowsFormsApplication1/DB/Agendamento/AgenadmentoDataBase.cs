﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Agendamento
{
    class AgenadmentoDataBase
    {
        public int Salvar(AgendamentoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido (IdAgendamento, nome_servico, preco_servico, forma_de_pagamento, nome_cliente, Data, Hora) 
                              VALUES (@IdAgendamento, @nome_servico, @preco_servico, @forma_de_pagamento, @nome_cliente, @Data, @Hora )";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Id", dto.IdAgendamento));
            parms.Add(new MySqlParameter("nome_servico", dto.nome_servico));
            parms.Add(new MySqlParameter("preco_servico", dto.preco_servico));
            parms.Add(new MySqlParameter("forma_de_pagamento", dto.forma_de_pagamento));
            parms.Add(new MySqlParameter("nome_cliente", dto.nome_cliente));
            parms.Add(new MySqlParameter("Data", dto.Data));
            parms.Add(new MySqlParameter("Hora", dto.Hora));
            parms.Add(new MySqlParameter(" idServicos ", dto.idServicos));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }


        public void Remover(int id)
        {
            string script = @"DELETE FROM Agendamento WHERE IdAgendamento = @IdAgendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("IdAgendamento", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<AgendamentoConsultarView> Consultar(string cliente)
        {
            string script = @"SELECT * FROM AgendamentoConsultarView  WHERE Agendamento like @Agendamento";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Agendamento", cliente + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<AgendamentoConsultarView> lista = new List<AgendamentoConsultarView>();
            while (reader.Read())
            {
                AgendamentoConsultarView dto = new AgendamentoConsultarView();
                dto.IdAgendamento = reader.GetInt32("IdAgendamento");
                dto.nome_servico = reader.GetString("nome_servico");
                dto.preco_servico = reader.GetString("preco_servico");
                dto.forma_de_pagamento = reader.GetString("forma_de_pagamento");
                dto.nome_cliente = reader.GetString("nome_cliente");
                dto.Data = reader.GetString("Data");
                dto.Hora = reader.GetString("Hora");
                dto.Hora = reader.GetString("idServicos");


                lista.Add(dto);

            }

            reader.Close();

            return lista;
        }
    }
}
