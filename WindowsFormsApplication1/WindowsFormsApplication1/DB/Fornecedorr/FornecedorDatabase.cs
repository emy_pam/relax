﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication1.DB.Base;

namespace WindowsFormsApplication1.DB.Fornecedorr
{
    class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO Fornecedores (Nome_fornecedor,Endereco,CNPJ,Telefone,Email,Estado,CEP) VALUES (@Nome_fornecedor,@Endereco,@CNPJ,@Telefone,@Email,@Estado,@CEP)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_fornecedor", dto.Nome_fornecedor));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Email", dto.Email));
            parms.Add(new MySqlParameter("Estado", dto.Cidade));
            parms.Add(new MySqlParameter("CEP", dto.CEP));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE Fornecedores
                                 SET  Nome_fornecedor = @Nome_fornecedor,
                                      Endereco   = @Endereco,
                                      CNPJ = @CNPJ,
                                      Telefone = @Telefone,
                                      Email = @Email,
                                      Estado = @Estado,
                                      CEP = @CEP
                               WHERE idFornecedores = @idFornecedores";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_fornecedor", dto.Nome_fornecedor));
            parms.Add(new MySqlParameter("Endereco", dto.Endereco));
            parms.Add(new MySqlParameter("CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Email", dto.Email));
            parms.Add(new MySqlParameter("Estado", dto.Cidade));
            parms.Add(new MySqlParameter("CEP", dto.CEP));


            Database db = new Database();
            db.ExecuteInsertScript(script, parms);


        }
       
   
        public void Remover(int id)
        {
            string script = @"DELETE FROM Fornecedores WHERE idFornecedores = @idFornecedores";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idFornecedores", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }
        public List<FornecedorDTO> Consultar(string produto)
        {
            string script = @"SELECT * FROM Fornecedores WHERE Nome_fornecedor like @Nome_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Nome_fornecedor", produto + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.id = reader.GetInt32("idFornecedores");
                dto.Nome_fornecedor = reader.GetString("Nome_fornecedor");
                dto.Endereco = reader.GetString("Endereco");
                dto.CNPJ = reader.GetString("CNPJ");
                dto.Telefone = reader.GetString("Telefone");
                dto.Email = reader.GetString("Email");
                dto.Cidade = reader.GetString("Estado");
                dto.CEP = reader.GetString("CEP");

                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM Fornecedores";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.id = reader.GetInt32("idFornecedores");
                dto.Nome_fornecedor = reader.GetString("Nome_fornecedor");
                dto.Endereco = reader.GetString("Endereco");
                dto.CNPJ = reader.GetString("CNPJ");
                dto.Telefone = reader.GetString("Telefone");
                dto.Email = reader.GetString("Email");
                dto.Cidade = reader.GetString("Estado");
                dto.CEP = reader.GetString("CEP");

                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }






    }
    
}
