﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Fornecedorr
{
    class FornecedorBusiness
    {
        public int Salvar(FornecedorDTO dto)
        {

            if (dto.Nome_fornecedor == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.CNPJ == string.Empty)
            {
                throw new ArgumentException("CNPJ é obrigatório.");
            }

            if (dto.Email == string.Empty)
            {
                throw new ArgumentException("Email é obrigatório.");
            }

            if (dto.Endereco == string.Empty)
            {
                throw new ArgumentException("Endereco é obrigatório.");
            }

            if (dto.Telefone == string.Empty)
            {
                throw new ArgumentException("Telefone é obrigatório.");
            }

            FornecedorDatabase db = new FornecedorDatabase();
            return db.Salvar(dto);

        }

        public void Remover(int idFornecedores)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Remover(idFornecedores);
        }

        public void Alterar(FornecedorDTO dto)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            db.Alterar(dto);
        }

        public List<FornecedorDTO> Consultar(string produto)
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Consultar(produto);
        }

        public List<FornecedorDTO> Listar()
        {
            FornecedorDatabase db = new FornecedorDatabase();
            return db.Listar();
        }


    }
}
