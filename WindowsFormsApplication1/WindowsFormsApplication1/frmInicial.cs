﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.DB.CadastroProduto;
using WindowsFormsApplication1.DB.Folha_de_Pagamento_s;
using WindowsFormsApplication1.DB.Pedido;

namespace WindowsFormsApplication1
{
    public partial class frmInicial : Form
    {
        public frmInicial()
        {
            InitializeComponent();
            
           
        }

       

        private void frmInicial_Load(object sender, EventArgs e)
        {
           
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Form3 tela = new Form3();
            tela.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Folha tela = new Folha();
            tela.Show();
        }
        private void TELA (UserControl control)
        {
            if (panel3.Controls.Count == 1)
                panel3.Controls.RemoveAt(0);
            panel3.Controls.Add(control);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            UserControl1 tela = new UserControl1();
            TELA(tela);

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
           
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Hide();
            Fornecedor tela = new Fornecedor();
            tela.Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Cadastro_de_FuncionarioNovo tela = new Cadastro_de_FuncionarioNovo();
            tela.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form1 te = new Form1();
            te.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Hide();
            Cadastro_de_Produto Produto = new Cadastro_de_Produto();
            Produto.Show();

        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            Pedido teka = new Pedido();
            TELA(teka);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            this.Hide();
            ConsultaProdutos tela = new ConsultaProdutos();
            tela.Show();
        }
    }
}
