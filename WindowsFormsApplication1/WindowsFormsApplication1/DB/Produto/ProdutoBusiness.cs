﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }

            ProdutoDataBase db = new ProdutoDataBase();
            return db.Salvar(dto);
        }


        public void Alterar(ProdutoDTO dto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            db.Remover(id);

        }


        public List<ProdutoDTO> Consultar(string produto)
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Consultar(produto);
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDataBase db = new ProdutoDataBase();
            return db.Listar();
        }






    }
}
